package com.practice.webpos.service.customer;

import com.practice.webpos.dto.customer.CustomerDto;

public interface CustomerService {
	CustomerDto create(CustomerDto customerDto);
	CustomerDto findOne(long id);
	CustomerDto update(CustomerDto customerDto);
	CustomerDto findByCustomerCodeLikeAndStatusTrue(String customerCode);
	CustomerDto findByCustomerCodeLikeAndStatusFalse(String customerCode);
	CustomerDto setActive(long id, boolean isActive);
}
