package com.practice.webpos.service.customer;

import com.practice.webpos.dto.customer.CreditTermsDto;

public interface CreditTermsService {

	CreditTermsDto create(CreditTermsDto creditTermsDto);
}
