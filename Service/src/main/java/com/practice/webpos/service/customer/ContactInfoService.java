package com.practice.webpos.service.customer;

import com.practice.webpos.dto.customer.ContactInfoDto;

public interface ContactInfoService {
	ContactInfoDto create(ContactInfoDto contactInfoDto);
}
