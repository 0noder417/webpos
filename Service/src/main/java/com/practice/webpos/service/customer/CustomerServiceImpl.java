package com.practice.webpos.service.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.webpos.dto.customer.CustomerDto;
import com.practice.webpos.entities.customer.ContactInfo;
import com.practice.webpos.entities.customer.CreditTerms;
import com.practice.webpos.entities.customer.Customer;
import com.practice.webpos.mapper.customer.ContactInfoMapper;
import com.practice.webpos.mapper.customer.CreditTermsMapper;
import com.practice.webpos.mapper.customer.CustomerMapper;
import com.practice.webpos.repository.customer.ContactInfoRepository;
import com.practice.webpos.repository.customer.CustomerRepository;

@Service
class CustomerServiceImpl implements CustomerService {
	
	private CustomerRepository customerRepository;
	
	@Autowired
	public CustomerServiceImpl(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}
	
	@Override
	public CustomerDto create(CustomerDto customerDto) {
		Customer customer = CustomerMapper.toEntity(customerDto);
		customer = customerRepository.save(customer);
		return CustomerMapper.toDto(customer);
	}
	
	@Override
	public CustomerDto findOne(long id) {
		Customer customer = customerRepository.findOne(id);
		return CustomerMapper.toDto(customer);
	}
	
	@Override
	public CustomerDto update(CustomerDto customerDto) {
		Customer customer = CustomerMapper.toEntity(customerDto);
		customer = customerRepository.save(customer);
		return CustomerMapper.toDto(customer);
	}
	
	@Override
	public CustomerDto findByCustomerCodeLikeAndStatusTrue(String customerCode) {
		Customer customer = customerRepository.findByCustomerCodeLikeAndStatusTrue(customerCode);
		return CustomerMapper.toDto(customer);
	}
	
	@Override
	public CustomerDto findByCustomerCodeLikeAndStatusFalse(String customerCode) {
		Customer customer = customerRepository.findByCustomerCodeLikeAndStatusFalse(customerCode);
		return CustomerMapper.toDto(customer);
	}
	
	@Override
	public CustomerDto setActive(long id, boolean isActive) {
		Customer customer = customerRepository.findOne(id);
		
		if (isActive) {
			customer.setStatus(true);
		} else {
			customer.setStatus(false);
		}

		customer = customerRepository.save(customer);
		return CustomerMapper.toDto(customer);
	}
}
