package com.practice.webpos.service.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.webpos.dto.customer.CreditTermsDto;
import com.practice.webpos.entities.customer.CreditTerms;
import com.practice.webpos.mapper.customer.CreditTermsMapper;
import com.practice.webpos.repository.customer.CreditTermsRepository;

@Service
public class CreditTermsServiceImpl implements CreditTermsService {

	private CreditTermsRepository creditTermsRepository;
	
	@Autowired
	public CreditTermsServiceImpl(CreditTermsRepository creditTermsRepository) {
		this.creditTermsRepository = creditTermsRepository;
	}
	
	@Override
	public CreditTermsDto create(CreditTermsDto creditTermsDto) {
		CreditTerms creditTerms = CreditTermsMapper.toEntity(creditTermsDto);
		creditTerms = creditTermsRepository.save(creditTerms);
		return CreditTermsMapper.toDto(creditTerms);
	}
	
}
