package com.practice.webpos.service.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.webpos.dto.customer.ContactInfoDto;
import com.practice.webpos.entities.customer.ContactInfo;
import com.practice.webpos.mapper.customer.ContactInfoMapper;
import com.practice.webpos.repository.customer.ContactInfoRepository;

@Service
public class ContactInfoServiceImpl implements ContactInfoService {
	
	private ContactInfoRepository contactInfoRepository;
	
	@Autowired
	public ContactInfoServiceImpl(ContactInfoRepository contactInfoRepository) {
		this.contactInfoRepository = contactInfoRepository;
	}
	
	@Override
	public ContactInfoDto create(ContactInfoDto contactInfoDto) {
		ContactInfo contactInfo = ContactInfoMapper.toEntity(contactInfoDto);
		contactInfo = contactInfoRepository.save(contactInfo);
		return ContactInfoMapper.toDto(contactInfo);
	}
	
}
