package com.practice.webpos.mapper.customer;

import com.practice.webpos.dto.customer.ContactInfoDto;
import com.practice.webpos.dto.customer.CreditTermsDto;
import com.practice.webpos.dto.customer.CustomerDto;
import com.practice.webpos.entities.customer.ContactInfo;
import com.practice.webpos.entities.customer.CreditTerms;
import com.practice.webpos.entities.customer.Customer;

public class CustomerMapper {

	public static Customer toEntity(CustomerDto customerDto) {
		if (customerDto == null) {
			return null;
		}

		Customer customer = new Customer();
		customer.setCompanyAddress(customerDto.getCompanyAddress());
		customer.setCompanyName(customerDto.getCompanyName());

		ContactInfoDto contactInfoDto = customerDto.getContactInfoDto();
		ContactInfo contactInfo = ContactInfoMapper.toEntity(contactInfoDto);
		customer.setContactInfo(contactInfo);

		CreditTermsDto creditTermsDto = customerDto.getCreditTermsDto();
		CreditTerms creditTerms = CreditTermsMapper.toEntity(creditTermsDto);
		customer.setCreditTerms(creditTerms);
		
		customer.setCustomerCode(customerDto.getCustomerCode());
		customer.setEmailAddress(customerDto.getEmailAddress());
		customer.setFaxNumber(customerDto.getFaxNumber());
		customer.setId(customerDto.getId());
		customer.setOwner(customerDto.getOwner());
		customer.setStatus(customerDto.getStatus());
		customer.setTinNumber(customerDto.getTinNumber());

		return customer;
	}

	public static CustomerDto toDto(Customer customer) {
		if (customer == null) {
			return null;
		}

		CustomerDto customerDto = new CustomerDto();
		customerDto.setCompanyAddress(customer.getCompanyAddress());
		customerDto.setCompanyName(customer.getCompanyName());
		
		ContactInfo contactInfo = customer.getContactInfo();
		ContactInfoDto contactInfoDto = ContactInfoMapper.toDto(contactInfo);
		customerDto.setContactInfoDto(contactInfoDto);
		
		CreditTerms creditTerms = customer.getCreditTerms();
		CreditTermsDto creditTermsDto = CreditTermsMapper.toDto(creditTerms);
		customerDto.setCreditTermsDto(creditTermsDto);
		
		customerDto.setCustomerCode(customer.getCustomerCode());
		customerDto.setEmailAddress(customer.getEmailAddress());
		customerDto.setFaxNumber(customer.getFaxNumber());
		customerDto.setId(customer.getId());
		customerDto.setOwner(customer.getOwner());
		customerDto.setStatus(customer.getStatus());
		customerDto.setTinNumber(customer.getTinNumber());

		return customerDto;
	}
	
}
