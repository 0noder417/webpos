package com.practice.webpos.mapper.customer;

import com.practice.webpos.dto.customer.ContactInfoDto;
import com.practice.webpos.entities.customer.ContactInfo;

public class ContactInfoMapper {

	public static ContactInfo toEntity(ContactInfoDto contactInfoDto) {
		if (contactInfoDto == null) {
			return null;
		}

		ContactInfo contactInfo = new ContactInfo();
		contactInfo.setContactName(contactInfoDto.getContactName());
		contactInfo.setContactNumbers(contactInfoDto.getContactNumbers());
		contactInfo.setId(contactInfoDto.getId());
		contactInfo.setPosition(contactInfoDto.getPosition());

		return contactInfo;
	}

	public static ContactInfoDto toDto(ContactInfo contactInfo) {
		if (contactInfo == null) {
			return null;
		}

		ContactInfoDto contactInfoDto = new ContactInfoDto();
		contactInfoDto.setContactName(contactInfo.getContactName());
		contactInfoDto.setContactNumbers(contactInfo.getContactNumbers());
		contactInfoDto.setId(contactInfo.getId());
		contactInfoDto.setPosition(contactInfo.getPosition());

		return contactInfoDto;
	}
	
}
