package com.practice.webpos.mapper.customer;

import com.practice.webpos.dto.customer.CreditTermsDto;
import com.practice.webpos.entities.customer.CreditTerms;

public class CreditTermsMapper {

	public static CreditTerms toEntity(CreditTermsDto creditTermsDto) {
		if (creditTermsDto == null) {
			return null;
		}

		CreditTerms creditTerms = new CreditTerms();
		creditTerms.setCreditLimit(creditTermsDto.getCreditLimit());
		creditTerms.setId(creditTermsDto.getId());
		creditTerms.setRemarks(creditTermsDto.getRemarks());
		creditTerms.setTerms(creditTermsDto.getTerms());
		creditTerms.setYearStarted(creditTermsDto.getYearStarted());

		return creditTerms;
	}

	public static CreditTermsDto toDto(CreditTerms creditTerms) {
		if (creditTerms == null) {
			return null;
		}

		CreditTermsDto creditTermsDto = new CreditTermsDto();
		creditTermsDto.setCreditLimit(creditTerms.getCreditLimit());
		creditTermsDto.setId(creditTerms.getId());
		creditTermsDto.setRemarks(creditTerms.getRemarks());
		creditTermsDto.setTerms(creditTerms.getTerms());
		creditTermsDto.setYearStarted(creditTerms.getYearStarted());

		return creditTermsDto;
	}

}
