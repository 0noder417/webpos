package com.practice.webpos.api.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.practice.webpos.dto.customer.CustomerDto;
import com.practice.webpos.service.customer.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerResource {
	
	private CustomerService customerService;

	@Autowired
	public CustomerResource(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public CustomerDto create(@RequestBody CustomerDto customerDto) {
		return customerService.create(customerDto);
	}
	
}
