package com.practice.webpos.test.integration.customer;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.practice.webpos.dto.customer.ContactInfoDto;
import com.practice.webpos.dto.customer.CreditTermsDto;
import com.practice.webpos.dto.customer.CustomerDto;
import com.practice.webpos.service.customer.CustomerService;
import com.practice.webpos.test.WebPosTest;

public class CustomerIntegrationTest extends WebPosTest {
	
	@Autowired private CustomerService customerService;
	
	@Test
	@Ignore
	public void createCustomerTest() {
		CustomerDto customerDto = new CustomerDto();
		
		customerDto.setCompanyAddress("SampleCompanyAddress");
		customerDto.setCompanyName("SampleCompanyName");
		customerDto.setContactInfoDto(createContactInfoDto());
		customerDto.setCreditTermsDto(createCreditTermsDto());
		customerDto.setCustomerCode("SampleCustomerCode");
		customerDto.setEmailAddress("SampleEmailAddress");
		customerDto.setFaxNumber("SampleFaxNumber");
		customerDto.setOwner("SampleOwner");
		customerDto.setStatus(true);
		customerDto.setTinNumber("SampleTinNumber");
		
		customerDto = customerService.create(customerDto);
		Assert.isTrue(customerDto.getId() != 0);
	}
	
	private ContactInfoDto createContactInfoDto() {
		ContactInfoDto contactInfoDto = new ContactInfoDto();
		contactInfoDto.setContactName("SampleContactName");
		
		Collection<String> contactNumbers = new ArrayList<>();
		
		contactNumbers.add("SampleContactNumber1");
		contactNumbers.add("SampleContactNumber2");
		contactNumbers.add("SampleContactNumber3");
		contactInfoDto.setContactNumbers(contactNumbers);
		
		contactInfoDto.setPosition("SamplePosition");
		return contactInfoDto;
	}
	
	private CreditTermsDto createCreditTermsDto() {
		CreditTermsDto creditTermsDto = new CreditTermsDto();
		creditTermsDto.setCreditLimit(0.25);
		creditTermsDto.setRemarks("SampleRemarks");
		creditTermsDto.setTerms("SampleTerms");
		creditTermsDto.setYearStarted(2016);
		return creditTermsDto;
	}
	
	@Test
	@Ignore
	public void updateCustomerTest() {		
		CustomerDto customerDto = customerService.findOne(1);
		Assert.isTrue(customerDto.getId() == 1);
		
		//ContactInfoDto contactInfoDto = customerDto.getContactInfoDto();
		//CreditTermsDto creditTermsDto = customerDto.getCreditTermsDto();
		ContactInfoDto contactInfoDto = updateContactInfoDto();
		CreditTermsDto creditTermsDto = updateCreditTermsDto();
		
		Assert.isTrue(contactInfoDto.getId() == 1);
		Assert.isTrue(creditTermsDto.getId() == 1);
		
		customerDto.setId(1);
		customerDto.setCompanyAddress("ChangedCompanyAddress");
		customerDto.setCompanyName("ChangedCompanyName");
		customerDto.setContactInfoDto(contactInfoDto);
		customerDto.setCreditTermsDto(creditTermsDto);
		customerDto.setCustomerCode("ChangedCustomerCode");
		customerDto.setEmailAddress("ChangedEmailAddress");
		customerDto.setFaxNumber("ChangedFaxNumber");
		customerDto.setOwner("ChangedOwner");
		customerDto.setStatus(false);
		customerDto.setTinNumber("ChangedTinNumber");
		
		customerDto = customerService.update(customerDto);
		
		Assert.isTrue(customerDto.getId() == 1);
		Assert.isTrue(customerDto.getContactInfoDto().getId() == 1);
		Assert.isTrue(customerDto.getCreditTermsDto().getId() == 1);
		Assert.isTrue(customerDto.getContactInfoDto().getContactName().equals("ChangedContactName"));
		//Assert.isTrue(customerDto.getCompanyAddress().equals("ChangedCompanyAddress"));
		//Assert.isTrue(customerDto.getCompanyName().equals("ChangedCompanyName"));
		//Assert.isTrue(customerDto.getCustomerCode().equals("ChangedCustomerCode"));
		//Assert.isTrue(customerDto.getEmailAddress().equals("ChangedEmailAddress"));
		//Assert.isTrue(customerDto.getFaxNumber().equals("ChangedFaxNumber"));
		//Assert.isTrue(customerDto.getOwner().equals("ChangedOwner"));
		//Assert.isTrue(customerDto.getStatus().equals("ChangedStatus"));
		//Assert.isTrue(customerDto.getTinNumber().equals("ChangedTinNumber"));
	}
		
	private ContactInfoDto updateContactInfoDto() {
		ContactInfoDto contactInfoDto = new ContactInfoDto();
		contactInfoDto.setId(1);
		contactInfoDto.setContactName("ChangedContactName");
		
		Collection<String> contactNumbers = new ArrayList<>();
		
		contactNumbers.add("ChangedContactNumber1");
		contactNumbers.add("ChangedContactNumber2");
		contactNumbers.add("ChangedContactNumber3");
		contactInfoDto.setContactNumbers(contactNumbers);
		
		contactInfoDto.setPosition("ChangedPosition");
		return contactInfoDto;
	}
	
	private CreditTermsDto updateCreditTermsDto() {
		CreditTermsDto creditTermsDto = new CreditTermsDto();
		creditTermsDto.setId(1);
		creditTermsDto.setCreditLimit(0.50);
		creditTermsDto.setRemarks("ChangedRemarks");
		creditTermsDto.setTerms("ChangedTerms");
		creditTermsDto.setYearStarted(2015);
		return creditTermsDto;
	}           
	
	@Test
	@Ignore
	public void findCustomerByCustomerCodeLikeAndStatusTrueTest() {
		CustomerDto customerDto = customerService.findByCustomerCodeLikeAndStatusTrue("SampleCustomerCode");
		Assert.isTrue(customerDto != null);
	}
	
	@Test
	@Ignore
	public void findCustomerByCustomerCodeLikeAndStatusFalseTest() {
		CustomerDto customerDto = customerService.findByCustomerCodeLikeAndStatusFalse("ChangedCustomerCode");
		Assert.isTrue(customerDto != null);
	}
	
	@Test
	@Ignore
	public void findOneTest() {
		CustomerDto customerDto = customerService.findOne(1);
		Assert.isTrue(customerDto != null);
	}
	
	@Test
	public void setActiveTest() {
		CustomerDto customerDto = customerService.setActive(1, true);
		Assert.isTrue(customerDto.getCompanyAddress().equals("ChangedCompanyAddress"));
		Assert.isTrue(customerDto.getStatus());
	}
	
	@Test
	public void setInactiveTest() {
		CustomerDto customerDto = customerService.setActive(1, false);
		Assert.isTrue(customerDto.getCompanyAddress().equals("ChangedCompanyAddress"));
		Assert.isTrue(!customerDto.getStatus());
	}
}
