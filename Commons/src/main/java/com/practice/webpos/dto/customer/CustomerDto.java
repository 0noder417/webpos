package com.practice.webpos.dto.customer;

public class CustomerDto {

	private long id;
	private String customerCode;
	private String companyName;
	private String companyAddress;
	private String tinNumber;
	private String faxNumber;
	private String emailAddress;
	private String owner;
	private boolean status;
	private ContactInfoDto contactInfoDto;
	private CreditTermsDto creditTermsDto;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getTinNumber() {
		return tinNumber;
	}

	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public ContactInfoDto getContactInfoDto() {
		return contactInfoDto;
	}

	public void setContactInfoDto(ContactInfoDto contactInfoDto) {
		this.contactInfoDto = contactInfoDto;
	}

	public CreditTermsDto getCreditTermsDto() {
		return creditTermsDto;
	}

	public void setCreditTermsDto(CreditTermsDto creditTermsDto) {
		this.creditTermsDto = creditTermsDto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customerCode == null) ? 0 : customerCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerDto other = (CustomerDto) obj;
		if (customerCode == null) {
			if (other.customerCode != null)
				return false;
		} else if (!customerCode.equals(other.customerCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CustomerDto [id=" + id + ", customerCode=" + customerCode + ", companyName=" + companyName
				+ ", companyAddress=" + companyAddress + ", tinNumber=" + tinNumber + ", faxNumber=" + faxNumber
				+ ", emailAddress=" + emailAddress + ", owner=" + owner + ", status=" + status + "]";
	}

}
