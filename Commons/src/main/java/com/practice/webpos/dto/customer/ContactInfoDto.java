package com.practice.webpos.dto.customer;

import java.util.Collection;

public class ContactInfoDto {

	private long id;
	private String contactName;
	private String position;
	private Collection<String> contactNumbers;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Collection<String> getContactNumbers() {
		return contactNumbers;
	}

	public void setContactNumbers(Collection<String> contactNumbers) {
		this.contactNumbers = contactNumbers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contactName == null) ? 0 : contactName.hashCode());
		result = prime * result + ((contactNumbers == null) ? 0 : contactNumbers.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactInfoDto other = (ContactInfoDto) obj;
		if (contactName == null) {
			if (other.contactName != null)
				return false;
		} else if (!contactName.equals(other.contactName))
			return false;
		if (contactNumbers == null) {
			if (other.contactNumbers != null)
				return false;
		} else if (!contactNumbers.equals(other.contactNumbers))
			return false;
		if (id != other.id)
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ContactInfoDto [id=" + id + ", contactName=" + contactName + ", position=" + position
				+ ", contactNumbers=" + contactNumbers + "]";
	}

}
