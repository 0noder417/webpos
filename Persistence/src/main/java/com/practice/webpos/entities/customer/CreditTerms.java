package com.practice.webpos.entities.customer;

import javax.persistence.Entity;

import com.practice.webpos.entities.AbstractEntity;

@Entity
public class CreditTerms extends AbstractEntity {

	private static final long serialVersionUID = 3499339767096814135L;

	private double creditLimit;
	private String terms;
	private int yearStarted;
	private String remarks;
	
	public double getCreditLimit() {
		return creditLimit;
	}
	
	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}
	
	public String getTerms() {
		return terms;
	}
	
	public void setTerms(String terms) {
		this.terms = terms;
	}
	
	public int getYearStarted() {
		return yearStarted;
	}
	
	public void setYearStarted(int yearStarted) {
		this.yearStarted = yearStarted;
	}
	
	public String getRemarks() {
		return remarks;
	}
	
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(creditLimit);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((remarks == null) ? 0 : remarks.hashCode());
		result = prime * result + ((terms == null) ? 0 : terms.hashCode());
		result = prime * result + yearStarted;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CreditTerms other = (CreditTerms) obj;
		if (Double.doubleToLongBits(creditLimit) != Double.doubleToLongBits(other.creditLimit))
			return false;
		if (remarks == null) {
			if (other.remarks != null)
				return false;
		} else if (!remarks.equals(other.remarks))
			return false;
		if (terms == null) {
			if (other.terms != null)
				return false;
		} else if (!terms.equals(other.terms))
			return false;
		if (yearStarted != other.yearStarted)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CreditTerms [creditLimit=" + creditLimit + ", terms=" + terms + ", yearStarted=" + yearStarted
				+ ", remarks=" + remarks + "]";
	}
	
}
