package com.practice.webpos.entities.customer;

import java.util.Collection;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;

import com.practice.webpos.entities.AbstractEntity;

@Entity
public class ContactInfo extends AbstractEntity {

	private static final long serialVersionUID = -1459998464882745923L;
	
	private String contactName;
	private String position;
	
	@ElementCollection
	private Collection<String> contactNumbers;
	
	public String getContactName() {
		return contactName;
	}
	
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	
	public String getPosition() {
		return position;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}

	public Collection<String> getContactNumbers() {
		return contactNumbers;
	}

	public void setContactNumbers(Collection<String> contactNumbers) {
		this.contactNumbers = contactNumbers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((contactName == null) ? 0 : contactName.hashCode());
		result = prime * result + ((contactNumbers == null) ? 0 : contactNumbers.hashCode());
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactInfo other = (ContactInfo) obj;
		if (contactName == null) {
			if (other.contactName != null)
				return false;
		} else if (!contactName.equals(other.contactName))
			return false;
		if (contactNumbers == null) {
			if (other.contactNumbers != null)
				return false;
		} else if (!contactNumbers.equals(other.contactNumbers))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ContactInfo [contactName=" + contactName + ", position=" + position + ", contactNumbers="
				+ contactNumbers + "]";
	}
	
}
