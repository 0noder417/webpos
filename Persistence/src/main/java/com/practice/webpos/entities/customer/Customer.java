package com.practice.webpos.entities.customer;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.practice.webpos.entities.AbstractEntity;

@Entity
public class Customer extends AbstractEntity {

	private static final long serialVersionUID = -2388692200347488801L;
	
	private String customerCode;
	private String companyName;
	private String companyAddress;
	private String tinNumber;
	private String faxNumber;
	private String emailAddress;
	private String owner;
	private boolean status;
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn
	private ContactInfo contactInfo;
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn
	private CreditTerms creditTerms;

	public String getCustomerCode() {
		return customerCode;
	}
	
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getCompanyAddress() {
		return companyAddress;
	}
	
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	
	public String getTinNumber() {
		return tinNumber;
	}
	
	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}
	
	public ContactInfo getContactInfo() {
		return contactInfo;
	}
	
	public void setContactInfo(ContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}
	
	public String getFaxNumber() {
		return faxNumber;
	}
	
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public boolean getStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}

	public CreditTerms getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(CreditTerms creditTerms) {
		this.creditTerms = creditTerms;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((customerCode == null) ? 0 : customerCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (customerCode == null) {
			if (other.customerCode != null)
				return false;
		} else if (!customerCode.equals(other.customerCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Customer [customerCode=" + customerCode + ", companyName=" + companyName + ", companyAddress="
				+ companyAddress + ", tinNumber=" + tinNumber + ", contactInfo=" + contactInfo + ", faxNumber="
				+ faxNumber + ", emailAddress=" + emailAddress + ", owner=" + owner + ", status=" + status + "]";
	}
}
