package com.practice.webpos.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practice.webpos.entities.customer.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	Customer findByCustomerCodeLikeAndStatusTrue(String customerCode);
	Customer findByCustomerCodeLikeAndStatusFalse(String customerCode);
}
