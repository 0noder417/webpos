package com.practice.webpos.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practice.webpos.entities.customer.CreditTerms;

@Repository
public interface CreditTermsRepository extends JpaRepository<CreditTerms, Long> {
	
}
